package com.els.offer.offer.controller;

import java.net.URISyntaxException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.els.offer.offer.model.Offer;
import com.els.offer.offer.service.OfferService;

@RestController
@RequestMapping("/offer")
public class OfferControler {

	@Autowired
	OfferService offerService;
	
	
	@PostMapping("/create")
    public Offer createOffer(@RequestBody Offer offer) {
        
        Offer result = offerService.save(offer);
        return result;
    }
	
	
	@PutMapping("/update")
    public Offer updateOffer(@RequestBody Offer offer) {
        
        Offer result = offerService.update(offer);
        return result;
    }
	
	@GetMapping("/find/{id}")
    public Offer findOffer(@PathVariable Integer id) {
        
        Offer result = offerService.findOffer(id);
        return result;
    }
	
	
}
